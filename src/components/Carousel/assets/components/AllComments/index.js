import React, {useState, useCallback, useEffect} from 'react';
import cn from 'classnames';
import styles from './assets/styles.css';

import UserComments from '../UserComments';
import Info from '../Info'

const CommentBlockSize = 340;

export default function AllComments({ allCommentsShow, setACShow, Post, PictureInfo}){
    const PictureStyle = {
        backgroundImage: Post.picture.backgroundImage,
        backgroundSize: "100% 100%",
        width: PictureInfo.sizeX,
        height: PictureInfo.sizeY,
    }

    const PostBlockStyle = {
        width: PictureInfo.sizeX + CommentBlockSize,
        height: PictureInfo.sizeY
    }
    const PostInfoStyle = {
        height: PictureInfo.sizeY,
        width: CommentBlockSize
    }
    const CommentsStyle = {
        height: PictureInfo.sizeY - 70 - 150,
        width: CommentBlockSize,
    }
    const InfoStyle = {
        height: 90,
        width: CommentBlockSize
    }



    return(
        <>
            <div className={styles.wrapper}>
                <div className={styles.postBlock} style={PostBlockStyle}>
                    <div className={styles.picture} style={PictureStyle}></div>
                    <div className={styles.postInfo} style={PostInfoStyle}>
                        <div className={styles.author}>
                            <div className={styles.avatar} style={Post.authorAvatar}></div>
                            <div className={styles.name}>{Post.author}</div>
                            <div className={styles.point}>•</div>
                            <div className={styles.subscribe}>Подписки</div>
                        </div>
                        <div className={styles.comments} style={CommentsStyle}>
                            <UserComments Post={Post} />
                        </div>
                        <div className={styles.info} style={InfoStyle}>
                            <Info />
                        </div>
                    </div>
                </div>
                <div className={styles.bg} onClick={() => setACShow(false)}></div>
            </div>
        </>
    )
}