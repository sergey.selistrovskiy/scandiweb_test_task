import React, {useState} from 'react';
import cn from 'classnames';
import styles from './assets/styles.css';

import Carousel from '../Carousel';
import StoriesList from '../StoriesList';
import AllComments from '../Carousel/assets/components/AllComments';
import NavBar from '../Carousel/assets/components/NavBar';

const Account = {
    name: "Grogu0409",
    avatar: "url('./assets/images/Avatar.png')"
}

export default function Main(){
    const [allCommentsShow, setACShow] = useState(false);
    const wrapperStyleActive = { overflowY: "hidden"};
    const wrapperStyle = { overflowY: "scroll"};
    return(
        <>
        <div className={styles.navBar}>
            <NavBar Account={Account} />
        </div>
            <div className={cn(styles.wrapper)} style={allCommentsShow ? wrapperStyleActive : wrapperStyle}>
                
                <div className={styles.stories}>
                    <StoriesList />
                </div>
                <div className={styles.posts}>
                    <Carousel allCommentsShow={allCommentsShow} setACShow={setACShow} />
                </div>
            </div>

            
        </>
    )
}