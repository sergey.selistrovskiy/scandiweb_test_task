import React, {useCallback, useEffect, useRef, useState} from 'react';
import cn from 'classnames';
import styles from './assets/styles.css';
let id = 0;
let stories = [
    {
        authorAvatar: "url(https://picsum.photos/60/60/?random&t=" + id++ +")",
        authorName: "Mr. Kripper"
    },
    {
        authorAvatar: "url(https://picsum.photos/60/60/?random&t=" + id++ +")",
        authorName: "Serzzh_02"
    },
    {
        authorAvatar: "url(https://picsum.photos/60/60/?random&t=" + id++ +")",
        authorName: "ALEX_BUTOV2222222"
    },
    {
        authorAvatar: "url(https://picsum.photos/60/60/?random&t=" + id++ +")",
        authorName: "u7fs29"
    },
    {
        authorAvatar: "url(https://picsum.photos/60/60/?random&t=" + id++ +")",
        authorName: "js72sfd"
    },
    {
        authorAvatar: "url(https://picsum.photos/60/60/?random&t=" + id++ +")",
        authorName: "dr.Pepper"
    },
    {
        authorAvatar: "url(https://picsum.photos/60/60/?random&t=" + id++ +")",
        authorName: "sdasda12"
    },
    {
        authorAvatar: "url(https://picsum.photos/60/60/?random&t=" + id++ +")",
        authorName: "Kkkk"
    },
    {
        authorAvatar: "url(https://picsum.photos/60/60/?random&t=" + id++ +")",
        authorName: "gggg"
    },
    {
        authorAvatar: "url(https://picsum.photos/60/60/?random&t=" + id++ +")",
        authorName: "sllls"
    },
    {
        authorAvatar: "url(https://picsum.photos/60/60/?random&t=" + id++ +")",
        authorName: "sdnsf"
    },
    {
        authorAvatar: "url(https://picsum.photos/60/60/?random&t=" + id++ +")",
        authorName: "sl2222"
    },
];
let storiySize = 60; // 60px story and 30px margin-right
const StoriesSize = (stories.length - 2) * storiySize;
let position = 0;

let showLeftArrow = false;
let showRightArrow = true;

export default function StoriesList({ Post }){
    let ref = useRef();

    const [scrollPosition, setScrollPosition] = useState(0);
    let scrollStep = 90;

    const ArrowClick = useCallback((type) => {
        const el = ref.current;
        console.log(type);
        if(el){
            if(type === 'left'){
                if(position >= scrollStep){
                    position -= scrollStep;
                    setScrollPosition(position - scrollStep);
                }
            } else if(type === 'right'){
                if(position <= StoriesSize){
                    position += scrollStep;
                    setScrollPosition(position);
                }
            }
            el.scrollTo({
                left: position,
                behavior: 'smooth'
            });
        }

        if(position > 0) showLeftArrow = true;
        else showLeftArrow = false;
        if(position < StoriesSize - storiySize) showRightArrow = true;
        else showRightArrow = false

        console.log(position)
    }, [scrollPosition, setScrollPosition]);

    return(
        <>
            <div className={styles.mainBlock}>
                <div className={styles.storiesBlock} ref={ref}>
                    {
                        stories.map((v, idx) => {
                            const StoryAuthorAvatar = { backgroundImage: v.authorAvatar,
                                backgroundSize: "100% 100%",
                                backgroundRepeat: "no-repeat"
                            }
                            return(
                                <div className={styles.storyBlock}>
                                    <div className={styles.story} key={idx} style = {StoryAuthorAvatar}></div>
                                    <div className={styles.name}>{storyName(v.authorName)}</div>
                                </div>
                            )
                        })
                    }
                </div>
                <div className={styles.arrows}>
                    {showLeftArrow && (
                        <div className={cn(styles.button, styles.leftArrowBtn)} onClick={() => ArrowClick('left')}>{`<`}</div>
                    )}
                    {showRightArrow && (
                        <div className={cn(styles.button, styles.rightArrowBtn)} onClick={() => ArrowClick('right')}>{`>`}</div>

                    )}
                </div>
            </div>
        </>
    )
}





function storyName(name){
    const Symblos = 11;
    let newName = name.toLowerCase();
    if(name.length > Symblos){
        newName = (name.substr(0, Symblos) + "...").toLowerCase();
    }
    return newName;
}