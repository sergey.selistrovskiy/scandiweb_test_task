import React, {useState, useCallback, useEffect} from 'react';
import cn from 'classnames';
import styles from './assets/styles.css';


//let time = 0, timeBetween = Math.ceil(Math.abs(new Date - Post.comments[idx].commentDate.getTime()) / (1000 * 3600 * 24));

export default function UserComments({ Post }){
    return(
        <>
            {
                Post.comments.map((v, idx) => {
                    console.log("post", Post)
                    console.log(Post.comments[idx].commentAuthor)
                    let time = getTime(Post, idx);
                    const AvatarStyle = v.avatar;
                    return(
                    <div className={styles.post} key={idx}>
                        <div className={styles.commentAndAuthor}>
                            <div className={styles.avatar} style={AvatarStyle}></div>
                            <div className={styles.comment}><span>{Post.comments[idx].commentAuthor}</span><span>{v.comment}</span></div>
                        </div>
                        <div className={styles.commentInfo}>
                            <div className={styles.date}>{time}</div>
                            <div className={styles.likes}>"Нравится": {v.commentLikes}</div>
                            <div className={styles.reply}>Ответить</div>
                        </div>
                    </div>
                    )
                })
            }
        </>
    )
}


function getTime(Post, idx){
    let time = 0, timeBetween = Math.ceil(Math.abs(new Date - Post.comments[idx].commentDate.getTime()));;
    if(timeBetween > 86400000){
        time = Math.floor(Math.abs(new Date - Post.comments[idx].commentDate.getTime()) / (1000 * 3600 * 24)) + " д.";
    }
    else if(timeBetween >= 3600000 && timeBetween <= 86400000){
        time = Math.floor(Math.abs(new Date - Post.comments[idx].commentDate.getTime()) / (1000 * 3600)) + " ч.";
    }
    else if(timeBetween >= 60000 && timeBetween <= 3600000){
        time = Math.floor(Math.abs(new Date - Post.comments[idx].commentDate.getTime()) / (1000 * 60)) + " мин.";
    }
    else{
        time = Math.floor(Math.abs(new Date - Post.comments[idx].commentDate.getTime()) / (1000)) + " c.";
    }
    return time;
}