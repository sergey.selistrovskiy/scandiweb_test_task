import React, {useRef} from 'react';
import { BrowserRouter as Router, Link } from 'react-router-dom'
import cn from 'classnames';
import styles from './assets/styles.css';



export default function NavBar({ Account }){
    const InputRef = useRef();
    const AvatarStyle={
        backgroundImage: "url(https://www.postavy.cz/foto/grogu-foto.jpg)",
        backgroundSize: "100% 100%",
        backgroundRepeat: "no-repeat",
        borderRadius: "50%"
    }
    return(
        
            <nav>
                <Router>
                    <Link to={`/`} className={styles.Instagram}></Link>
                    <input className={styles.inputBlock} defaultValue={'Поиск'} ref={InputRef} onClick={() => clearDefaultValue(InputRef)}></input>
                    <div className={styles.icons}>

                        <div className={styles.house}></div>
                        <div className={styles.messenger}></div>
                        <div className={styles.compass}></div>
                        <div className={styles.heart}></div>

                        <div className={styles.avatar} style={AvatarStyle}> </div>
                    </div>
                </Router>
            </nav>
    )
}

function clearDefaultValue(InputRef){
    InputRef.current.value = ""
}