import React, {useState, useCallback, useEffect, useRef} from 'react';
import cn from 'classnames';
import styles from './assets/styles.css'

import AllComments from './assets/components/AllComments/';


const ViewsArray = [];

const POSTS = [
    {
        authorAvatar: "",
        author: "Harry Potter",
        date: "",
        authorComment: "Hahahaha rly funny guys, im think that is future, hahahahahah2ahahahahahahhahaha. Piu-piu piu-piu piu-piu piu-piu piu-piu hahahahahah2ahahahahahahhahaha Piu-piu piu-piu piu-piu piu-piu piu-piu",
        likes: 5225,
        picture:"",
        comments:[
            {
                avatar: "",
                comment: "Ha-ha-ha, rly funnyHa-ha-ha, rly funnyHa-ha-ha, rly funnyHa-ha-ha, rly funnyHa-ha-ha, rly funnyHa-ha-ha, rly funnyHa-ha-ha, rly funnyHa-ha-ha, rly funnyHa-ha-ha, rly funnyHa-ha-ha, rly funnyHa-ha-ha, rly funnyHa-ha-ha, rly funny",
                commentAuthor: "Olga_xxx",
                commentDate: new Date("2017.04.11 14:22"),
                commentLikes: Math.floor(Math.random() * 100 + 1)
            },
            {
                avatar: "",
                comment: "Heeh",
                commentAuthor: "Sergey W.",
                commentDate: new Date("2018.04.11 14:22"),
                commentLikes: Math.floor(Math.random() * 100 + 1)
            },
            {
                avatar: "",
                comment: "Hahahahaha",
                commentAuthor: "Mr. Brown",
                commentDate: new Date("2019.04.11 14:22"),
                commentLikes: Math.floor(Math.random() * 100 + 1)
            },
            {
                avatar: "",
                comment: "Omg..",
                commentAuthor: "Dr.Chuppy",
                commentDate: new Date("2020.04.11 14:22"),
                commentLikes: Math.floor(Math.random() * 100 + 1)
            },
            {
                avatar: "",
                comment: "Ha-ha-ha, rly funny",
                commentAuthor: "ReTaRd",
                commentDate: new Date("2010.05.11 14:22"),
                commentLikes: Math.floor(Math.random() * 100 + 1)
            },
            {
                avatar: "",
                comment: "Ha-ha-ha, rly funny",
                commentAuthor: "ReTaRd",
                commentDate: new Date("2020.05.12 14:22"),
                commentLikes: Math.floor(Math.random() * 100 + 1)
            },
            {
                avatar: "",
                comment: "Ha-ha-ha, rly funny",
                commentAuthor: "ReTaRd",
                commentDate: new Date("2021.05.16 14:22"),
                commentLikes: Math.floor(Math.random() * 100 + 1)
            },
            {
                avatar: "",
                comment: "Ha-ha-ha, rly funny",
                commentAuthor: "ReTaRd",
                commentDate: new Date("2021.04.11 14:22"),
                commentLikes: Math.floor(Math.random() * 100 + 1)
            }
        ]
    },
    {
        authorAvatar: "",
        author: "Petter I",
        date: "",
        authorComment: "123321123321123321123321123321123321123321123321123321123321123321123321123321123321123321123321123321123321123321123321123321123321123321123321123321123321123321123321123321123321123321123321123321123321123321123321123321123321123321123321123321123321",
        likes: 5246,

        comments:[
            {
                avatar: "",
                comment: "Ha-ha-ha, rly funny",
                commentAuthor: "Olga_xxx",
                commentDate: new Date("2020.05.12 14:22"),
                commentLikes: Math.floor(Math.random() * 100 + 1)
            },
            {
                avatar: "",
                comment: "Ha-ha-ha, rly funny",
                commentAuthor: "Olga_xxx",
                commentDate: new Date("2020.05.12 14:22"),
                commentLikes: Math.floor(Math.random() * 100 + 1)
            },
            {
                avatar: "",
                comment: "Ha-ha-ha, rly funny",
                commentAuthor: "Olga_xxx",
                commentDate: new Date("2020.05.12 14:22"),
                commentLikes: Math.floor(Math.random() * 100 + 1)
            }
        ]
    },
    {
        authorAvatar: "",
        author: "Petter I",
        date: "",
        authorComment: "123321123321123321123321123321123321123321123321123321123321123321123321123321123321123321123321123321123321123321123321123321123321123321123321123321123321123321123321123321123321123321123321123321123321123321123321123321123321123321123321123321123321",
        likes: 55869,

        comments:[
            {
                avatar: "",
                comment: "Ha-ha-ha, rly funny",
                commentAuthor: "Olga_xxx",
                commentDate: new Date("2020.05.12 14:22"),
                commentLikes: Math.floor(Math.random() * 100 + 1)
            },
            {
                avatar: "",
                comment: "Ha-ha-ha, rly funny",
                commentAuthor: "Olga_xxx",
                commentDate: new Date("2020.05.12 14:22"),
                commentLikes: Math.floor(Math.random() * 100 + 1)
            },
            {
                avatar: "",
                comment: "Ha-ha-ha, rly funny",
                commentAuthor: "Olga_xxx",
                commentDate: new Date("2020.05.12 14:22"),
                commentLikes: Math.floor(Math.random() * 100 + 1)
            }
        ]
    },
    {
        authorAvatar: "",
        author: "Petter I",
        date: "",
        authorComment: "123321123321123321123321123321123321123321123321123321123321123321123321123321123321123321123321123321123321123321123321123321123321123321123321123321123321123321123321123321123321123321123321123321123321123321123321123321123321123321123321123321123321",
        likes: 33354,

        comments:[
            {
                avatar: "",
                comment: "Ha-ha-ha, rly funny",
                commentAuthor: "Olga_xxx",
                commentDate: new Date("2020.05.12 14:22"),
                commentLikes: Math.floor(Math.random() * 100 + 1)
            }
        ]
    },
]


for(let i = 0; i < POSTS.length; i++){
    ViewsArray.push(Math.floor(Math.random() * (10000) + 200));
}


const PictureInfo ={ 
    id: 0,
    sizeX: 0,
    sizeY: 0,
    authorName: "",
}


export default function Carousel({ allCommentsShow, setACShow }){
    return(
        <>
        <div className={cn(styles.postsBlock)}>
            {
                POSTS.map((post, idx) => {
                    PictureInfo.sizeX = 614;
                    PictureInfo.sizeY = 660;
                    PictureInfo.authorName = post.author;
                    const AuthorAvatarStyle = { 
                    backgroundImage: `url(https://picsum.photos/40/40/?random&t=${idx})`,
                    backgroundSize: "100% 100%",
                    backgroundRepeat: "no-repeat"
                };
                    //const PostPictureStyle = { backgroundImage: `url(https://picsum.photos/${PictureInfo.sizeX}/${PictureInfo.sizeY}/?random&t=${idx})`, height: PictureInfo.sizeY};
                    const [sendActive, setSendActive] = useState(false);
                    const PostPictureStyle = { 
                        backgroundImage: `url(https://picsum.photos/1200/1000/?random&t=${idx})`, 
                        backgroundSize: "cover",
                        backgroundPosition: "center center"
                    };
                    POSTS[idx].authorAvatar = AuthorAvatarStyle;
                    POSTS[idx].picture = PostPictureStyle;
                    const Views = Math.floor(Math.random() * (10000) + 200);
                    let AuthorComment = comment(post.authorComment);
                    const [show, setShow] = useState(false);
                    let CommentsAmount = post.comments.length;
                    const Comment = post.comments;
                    const [likes, setLikes] = useState(post.likes);

                    POSTS[idx].comments.map((v, id) => {
                        const AuthorCommentAvatarStyle = { 
                            backgroundImage: `url(https://picsum.photos/40/40/?random&t=${(idx+1) * 1000 + id})`,
                            backgroundSize: "100% 100%",
                            backgroundRepeat: "no-repeat"
                    };
                        POSTS[idx].comments[id].avatar = AuthorCommentAvatarStyle;
                    })
                    console.log(POSTS);
                    
                    
                    return(
                        <div className={styles.postBlock} key={idx}>
                            <div className={styles.author}>
                                <div className={styles.authorLogo} style={AuthorAvatarStyle} />
                                <div className={styles.authorName}>{(post.author).toLowerCase()}</div>
                                <div className={styles.authorOther}></div>
                            </div>

                            <div className={styles.postPicture} style={PostPictureStyle} onDoubleClick={() => likes === post.likes ? setLikes(likes + 1) : setLikes(likes - 1)} />
                            <div className={styles.reaction}>
                                <div className={cn(styles.like, {[styles.likeActive]:(likes===post.likes+1)})} onClick={() => likes === post.likes ? setLikes(likes + 1) : setLikes(likes - 1)} />
                                <div className={styles.comment} />
                                <div className={styles.sendPost} />
                                <div className={styles.savePost} />
                            </div>
                            <div className={styles.views}><p>Просмотры: {ViewsArray[idx]}</p></div>
                            <div className={styles.postText}>
                                <div className={styles.firstPart}>
                                    <div className={styles.name}>{(post.author).toLowerCase()}</div>
                                    <div>{AuthorComment}</div>
                                    { !show && (
                                            <div onClick={() => setShow(!show)}>...</div>
                                    ) }
                                </div>
                                <div className={styles.secondPart}>
                                    { show && (
                                            <div>
                                                { showComment(post.authorComment) }
                                                <div onClick={() => setShow(!show)} className={styles.threePoints}>...</div>
                                            </div>
                                    ) }
                                </div>
                            </div>
                            
                            <div className={styles.likes}>
                                <div>
                                {likesInString(likes)} отметок "Нравится"
                                </div>
                            </div>

                            <div className={styles.lastComments}>
                            {
                                CommentsAmount >= 3 && ( <div className={styles.allComments} onClick={() => { setACShow(true); PictureInfo.id= idx } }>Посмотреть все комментарии ({CommentsAmount})</div> )
                            }
                                {
                                    CommentsAmount >= 1 && (
                                        <div><span className={styles.name}>{Comment[Comment.length - 1].commentAuthor}</span>: {Comment[Comment.length - 1].comment}</div>
                                    )
                                }
                                {
                                    CommentsAmount >= 2 && (
                                        <div><span className={styles.name}>{Comment[Comment.length - 2].commentAuthor}</span>: {Comment[Comment.length - 2].comment}</div>
                                    )
                                }
                                <div className={styles.postDate}>Post date</div>
                            </div>
                            <div className={styles.sendComment}>
                                <div>
                                    <div className={styles.smile} />
                                    <input type="text" className={styles.inputStyle} placeholder={"Добавьте комментарий..."} onChange={(e) => { if(e.target.value.length > 0) setSendActive(true); else setSendActive(false); console.log(SendActive)} } />
                                    {/* onChange={(e) => {if(e.target.value.length > 0) SendActive = true; else SendActive = false }  */}
                                </div> 
                                <div className={cn(styles.send)}>
                                    <div className={cn(styles.sendDeactive, {[styles.sendActive]: sendActive})}>Опубликовать</div>
                                </div>
                            </div>
                        </div>
                    )
                })
            }
            
        </div>
        { allCommentsShow && (
                <AllComments allCommentsShow={allCommentsShow} setACShow={setACShow} Post={POSTS[PictureInfo.id]} PictureInfo={PictureInfo}/>
            ) }
        </>
    )
}



function comment(text){
    let size = text.length;
    let newText = "";
    if(size > 60){
        for(let i = 0; i < 60; i++){
            console.log(text[i])
            newText += text[i];
        }
        console.log(newText)
    }
    else {
        newText = text
    }

    return newText;
}

function showComment(text) {
    let size = text.length;
    let newText = "";
    for(let i = 60; i < size; i++){
        console.log(text[i])
        newText += text[i];
    }
    return newText;
}

const likesInString = (v) => {
    let resLikes = "";
    let likes = v + "";
    likes = likes.split( /(?=(?:\d{3})+(?!\d))/);
    likes.map((value, id) => {
        resLikes += value + " ";
    });
    return resLikes;
}